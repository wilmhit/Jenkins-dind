FROM rancher/dind

RUN apt-get update
RUN apt-get install -y maven git openssh-server zip

RUN echo "root:toor" | chpasswd
COPY start.sh .

RUN chmod +x start.sh

RUN sed -i "s/PermitRootLogin without-password/PermitRootLogin yes/" /etc/ssh/sshd_config
RUN sed -i "s/AllowUsers/AllowUsers root/" /etc/ssh/sshd_config
RUN sed -i "s/UsePAM yes/UsePAM no/" /etc/ssh/sshd_config

RUN curl "https://download.java.net/openjdk/jdk11/ri/openjdk-11+28_linux-x64_bin.tar.gz" --output java11.tar.gz
RUN tar -xf java11.tar.gz 

ENV PATH=/jdk-11/bin:$PATH
ENV JAVA_HOME=/jdk11

CMD ["./start.sh"]
